<?php

namespace App\Helpers;

class Roles
{
    const IS_MANAGER = 1;
    const IS_CLIENT = 2;
}