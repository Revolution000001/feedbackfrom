<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailClass extends Mailable
{
    use Queueable, SerializesModels;

    private $subjectFeedback;
    private $messageFeedback;
    private $file;
    private $creation_time;
    private $name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject,$message,$file,$creation_time,$name)
    {
        $this->subjectFeedback = $subject;
        $this->messageFeedback = $message;
        $this->file = $file;
        $this->creation_time = $creation_time;
        $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.email')
            ->with([
                'subjectFeedback' => $this->subjectFeedback,
                'messageFeedback' => $this->messageFeedback,
                'name' => $this->name,
                'file' => $this->file,
                'creation_time' => $this->creation_time
            ])
            ->subject('Новое письмо');
    }
}
