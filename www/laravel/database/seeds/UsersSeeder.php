<?php

use App\Helpers\Roles;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('users')->count() != 0) {
            return;
        }

        DB::table('users')->insert([
            [
                'id' => 1,
                'name' => 'Manager',
                'email' => 'man@mail.ru',
                'password' => Hash::make('123456'),
                'api_token' => Str::random(60),
                'role_id' => Roles::IS_MANAGER,
                'created_at' => null,
                'updated_at' => null,
            ]

        ]);
    }
}
