<?php

namespace App\Http\Controllers;

use App\Application;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\StoreManagerApplication;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class ManagerController extends Controller
{
    public function index()
    {
        $application = Application::with('user')
            ->paginate(10);
        return $application;
    }

    public function store(StoreManagerApplication $request)
    {
        $application = Application::find($request->id);
        $this->authorize('update',$application);
        Application::where('id',$request->id)
            ->update(['viewed' => true]);
    }
}
