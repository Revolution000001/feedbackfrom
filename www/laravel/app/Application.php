<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Application extends Model
{
    protected $fillable = [
        'subject',
        'message',
        'url_img',
        'user_id',
        'viewed'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
