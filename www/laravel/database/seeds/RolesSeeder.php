<?php

use Illuminate\Database\Seeder;
use App\Helpers\Roles;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('roles')->count() != 0) {
            return;
        }

        DB::table('roles')->insert([
            [
                'id' => Roles::IS_MANAGER,
                'name' => 'менеджер',
                'created_at' => null,
                'updated_at' => null,
            ],
            [
                'id' => Roles::IS_CLIENT,
                'name' => 'клиент',
                'created_at' => null,
                'updated_at' => null,
            ]

        ]);
    }
}
