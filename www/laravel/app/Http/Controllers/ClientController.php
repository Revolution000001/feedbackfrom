<?php

namespace App\Http\Controllers;

use App\Application;
use App\Mail\MailClass;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\StoreClientApplication;

class ClientController extends Controller
{
    public function index()
    {
        $data = [
            'sent' => false
        ];
        $applicationCreated = Application::select('created_at')
            ->where('user_id', Auth::id())
            ->orderBy('created_at','desc')
            ->first();
        $applicationCreated = new Carbon($applicationCreated['created_at']);
        $applicationCreatedAddDay = $applicationCreated->addDay();
        $currentDate = Carbon::now();
        if($currentDate < $applicationCreatedAddDay){
            $data['sent'] = true;
        }
        return $data;
    }

    public function store(StoreClientApplication $request)
    {
        $user = User::where('api_token',$request->api_token)
            ->first();
        $this->authorize('create',$user);
        $file = "";
        $data = [
            'subject' => $request->subject,
            'message' => $request->message,
            'user_id' => $user->id,
            'viewed' => false,
            'url_img' => $file
        ];
        if($request->url_img != ""){
            $file = $request->file('url_img')
                ->store('public/img');
            $data['url_img'] = $file;
        }
        $applicationCreated = Application::create($data);

        Mail::to($user->email)
            ->queue(new MailClass($data['subject'], $data['message'], $file, $applicationCreated->created_at, $user->name));
        return json_encode('ok');
    }
}
