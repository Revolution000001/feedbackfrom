<?php

namespace App\Policies;

use App\Helpers\Roles;
use App\User;
use App\Application;
use Illuminate\Auth\Access\HandlesAuthorization;

class ApplicationPolicy
{
    use HandlesAuthorization;

    public function update(User $user)
    {
        return $user->role_id === Roles::IS_MANAGER;
    }

    public function create(User $user)
    {
        return $user->role_id === Roles::IS_CLIENT;
    }

}
