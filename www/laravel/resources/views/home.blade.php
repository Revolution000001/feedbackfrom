@extends('layouts.app')

@section('content')
    @if(Auth::user()->role_id == \App\Helpers\Roles::IS_MANAGER)
        <from-manager token={{Auth::user()->api_token}}></from-manager>
    @else
        <from-client token={{Auth::user()->api_token}}></from-client>
    @endif
@endsection
