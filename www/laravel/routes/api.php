<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['prefix' => 'manager', 'middleware' => 'auth:api'], function () {
    Route::ApiResource('home', 'ManagerController',
        ['except' => ['show', 'update', 'destroy']]);
});


Route::group(['prefix' => 'client', 'middleware' => 'auth:api'], function () {
    Route::ApiResource('home', 'ClientController',
        ['except' => ['show', 'update', 'destroy']]);
});